//
//  FindBicycleViewController.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 28/10/2018.
//  Copyright © 2018 Marko Lehtola. All rights reserved.
//

import UIKit

class FindBicycleViewController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField! //sarjanumeron etsintäruutu
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func findButtonAction(_ sender: UIButton) {
        //TODO kun etsi painiketta painetaan, etsitään polkupyörä tietokannasta
        searchTextField.text = "löytyi"
    }

}
