//
//  LoginPageViewController.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 28/10/2018.
//  Copyright © 2018 Marko Lehtola. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class LoginPageViewController: UIViewController {

    @IBOutlet weak var signinSelector: UISegmentedControl!
    @IBOutlet weak var signinLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signinButton: UIButton!
    //tietokanta muuttuja
    var ref: DatabaseReference!
    
    var isSignIn:Bool = true
    
    override func loadView(){
        //katsotaan onko käyttäjä olemassa UserDefaultsissa, eli tallennettu viime kerrasta
        retrieveUser()
        super.loadView()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //yhteys tietokantaan määritellään täsä
        ref = Database.database().reference()
    }
    
    @IBAction func signinSelectorChanged(_ sender: UISegmentedControl) {
        //käännä boolean
        isSignIn = !isSignIn
        
        //tsekkaa missä asennossa painike on ja muuta otsikko ja nappulan teksti
        if isSignIn {
            signinLabel.text = "Kirjaudu"
            signinButton.setTitle("Kirjaudu", for: .normal)
        } else {
            signinLabel.text = "Rekisteröidy"
            signinButton.setTitle("Rekisteröidy", for: .normal)
        }
    }
    
    @IBAction func signinButtonAction(_ sender: UIButton) {
        //TODO:katsotaan että salasana ja käyttäjätunnus eivät ole tyhjiä
        
        if let email = emailTextField.text, let password = passwordTextField.text{
            
            // tsekkaa ollaanko kirjautumassa vai rekisteröitymässä
            
            if isSignIn {
                Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                    if user != nil {
                        //käyttäjä löytyi
                        self.saveUser(password: password, email: email)
                        self.performSegue(withIdentifier: "goToHome", sender: self)
                    } else {
                        //TODO: chekkaa virhe ja näytä virhe
                    }
                }
            } else {
                Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
                    
                    //chekataan ettei käyttäjä ole nil
                    if (authResult?.user) != nil {
                        //käyttäjä löytyi
                        self.saveUser(password: password, email: email)
                        //lisätään käyttäjä tietokantaan
                        self.ref.child("users").child(Auth.auth().currentUser!.uid).setValue(["shareInfo": false])
                        self.performSegue(withIdentifier: "goToHome", sender: self)
                    } else {
                        //TODO: virhe, näytä virheviesti
                    }
                }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //piilotetaan näppäimistö kun poistuttaan ruudusta
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }

    func saveUser(password:String, email:String){
        //Tallennetaan käyttäjänimi ja salasana ensi kertaa varten
        let defaults = UserDefaults.standard
        defaults.set(email as String, forKey: "email")
        defaults.set(password as String, forKey: "password")
        
    }
    
    func retrieveUser(){
        //alustetaan userDefaults
        let defaults = UserDefaults.standard
        
        //haetaan email ja password ja jos löytyvät UserDefaultista, niin suoritetaan sisäänkirjaus
        if let mail = defaults.string(forKey: "email"), let passWord = defaults.string(forKey: "password"){
          
            //suoritetaan sisäänkirjaus
            Auth.auth().signIn(withEmail: mail, password: passWord) { (user, error) in
                //jos käyttäjä ei ole nil
                if user != nil {
                    //jos käyttäjä löytyi
                    self.performSegue(withIdentifier: "goToHome", sender: self)
                } else {
                    //TODO: chekkaa virhe ja näytä virhe
                }
            }
        }
    }
}

