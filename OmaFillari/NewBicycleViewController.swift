//
//  NewBicycleViewController.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 09/11/2018.
//  Copyright © 2018 Marko Lehtola. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage


class NewBicycleViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var imageView: UIImageView! //polkupyörän kuva
    @IBOutlet weak var frameNumber: UITextField!
    @IBOutlet weak var serialNumberTextField: UITextField!
    @IBOutlet weak var brandTextField: UITextField!
    @IBOutlet weak var modelTextField: UITextField!
    
    
    //määritellään tietokantayhteys
    var ref: DatabaseReference!
    
    let filename = "bicycle.jpg" //valokuvan tiedostonimi
    
    var imageReference: StorageReference {
        return Storage.storage().reference().child("bicycleImages") //polku valokuvalle
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //yhdistetään tietokantaan
        self.ref = Database.database().reference()
        
        /* ladataan kuva
        let downloadImageRef = imageReference.child(filename)
        
        let downloadtask = downloadImageRef.getData(maxSize: 1024 * 1024 * 12){(data, error) in
            if let data = data {
                 let image = UIImage(data: data)
                 self.imageView.image = image
            }
            print(error ?? "NO ERROR")
        }
        downloadtask.observe(.progress) { (snapshot) in
            print(snapshot.progress ?? "NO MORE PROGRESS")
        }
        downloadtask.resume()
        */
    }

    @IBAction func chooseImage(_ sender: Any) {
        
        //tehdään kuvan valinta kontrolleri
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Valokuvan lähde", message: "Valitse valokuvan lähde", preferredStyle: .actionSheet)
        //otetaan kuva kamerasta
        actionSheet.addAction(UIAlertAction(title: "Kamera", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        //otetaan kuva galleriasta
        actionSheet.addAction(UIAlertAction(title: "Valokuva kirjastosta", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        //peruutetaan kuvan otto
        actionSheet.addAction(UIAlertAction(title: "Peruuta", style: .cancel, handler:  nil ))
        
        //näytetään toiminto
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        imageView.image = image
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SaveNewBicycleButtonAction(_ sender: UIBarButtonItem) {
        
        
        //lisätään polkupyörä tietokantaan
        //tarkistetaan mikä on käyttäjän userID
        let user = Auth.auth().currentUser!.uid
        //katsotaan uuden pyörän runkonumero
        let serial = serialNumberTextField.text
        //tehdään mallille muuttuja
        let model = modelTextField.text
        //tehdään merkille muuttuja
        let brand = brandTextField.text
        
        //lisätään pyörä tietokantaan
        addBicycle(user: user, serial: serial!, brand: brand!, model: model!)
        //lisätään kuva palvelimelle
        addPictureToStorage(fileName: serial!)
        
        //siirrytään takaisin päävalikkoon
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    func addPictureToStorage(fileName:String){
        
        //Lisätään kuva tietokantaan
        guard let image = imageView.image else {return}
        guard let imageData = image.jpegData(compressionQuality: 0) else {return}//kuvan laatu 0-1 yhden ollessa paras
        
        let uploadImageRef = imageReference.child(fileName + ".jpg")
        
        let uploadTask = uploadImageRef.putData(imageData, metadata: nil) { (metadata,error) in
            print("UPLOAD TASK FINISHED")
            print(metadata ?? "FILLARI")
            print ( error ?? "NO ERROR")
            //TODO: tee referenssi
        }
        
        uploadTask.observe(.progress) {(snapshot) in
            print(snapshot.progress ?? "NO MORE PROGRESS")
        }
        
        uploadTask.resume()
        
    }
    
    
    
    func addBicycle(user:String, serial:String, brand:String, model:String){
       
        
        let ref = Database.database().reference()

        //lisätään pyörä tietokantaan
        ref.child("bicycles").child(serial).setValue(["brand":brand, "model":model])
        
        //tehdään linkitys bicyclesByUsers
        ref.child("bicyclesByUsers").child(user).childByAutoId().setValue(serial)
        
        //tehdään linkitys usersByBicycles
        ref.child("usersByBicycles").child(serial).childByAutoId().setValue(user)
        
    }

}
