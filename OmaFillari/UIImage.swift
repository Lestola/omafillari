//
//  UIImage.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 30/10/2018.
//  Copyright © 2018 Marko Lehtola. All rights reserved.
//

import UIKit

extension UIImage {
    
    
    class func scaleImageToSize(img: UIImage, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        
        img.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return scaledImage!
    }
    
}
