//
//  profileViewController.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 29/10/2018.
//  Copyright © 2018 Marko Lehtola. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class profileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet var imgProfilePicture: UIImageView! //profiilikuva
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var postalnumberTextField: UITextField!
    @IBOutlet weak var allowShareMyInformationSwitchButton: UISwitch!
    
    let filename = Auth.auth().currentUser!.uid + ".jpg" //valokuvan tiedostonimi käyttäjätunnus.jpg
    
    var imageReference: StorageReference {
        return Storage.storage().reference().child("profileImages") //polku valokuvalle
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //pyöristetään profiilikuvan kulmat
        imgProfilePicture.roundedImage()
        getOwnInformation()
    }
    
    @IBAction func profilePictureButtonAction(_ sender: UIButton) {
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        self.present(image, animated: true){
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            imgProfilePicture.image = image
        }
        else{
            //error message
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonAction(_ sender: UIBarButtonItem) {
        let name = nameTextField.text                                   //nimi
        let address = addressTextField.text                             //osoite
        let city = cityTextField.text                                   //kaupunki
        let postalNumber = postalnumberTextField.text                   //postinumero
        let phoneNumber = phoneNumberTextField.text                     //puhelinnumero
        let shareInfo = allowShareMyInformationSwitchButton.isOn   //jaetaanko tiedot
        addPictureToStorage()
        //lisätään käyttäjä tietokantaan
        let ref = Database.database().reference()
        if name != nil && address != nil && phoneNumber != nil{
            ref.child("users").child(Auth.auth().currentUser!.uid).setValue(["name":name, "address":address, "phoneNumber":phoneNumber,"city":city,"postalNumber":postalNumber, "shareInfo":shareInfo])
            
            //siirrytään takaisin päävalikkoon
            _ = navigationController?.popToRootViewController(animated: true)
            
        } else {
            //TODO: jos kaikki kentät eivät ole täytetty
        }
        
    }
    
    func getOwnInformation() {
        let ref = Database.database().reference()
        let userID = Auth.auth().currentUser?.uid
        
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let name = value?["name"] as? String ?? ""
            let address = value?["address"] as? String ?? ""
            let city = value?["city"] as? String ?? ""
            let postalNumber = value?["postalNumber"] as? String ?? ""
            let phoneNumber = value?["phoneNumber"] as? String ?? ""
            let allowShareInfo = value?["shareInfo"] as? Bool ?? true
            self.nameTextField.text = name
            self.addressTextField.text = address
            self.phoneNumberTextField.text = phoneNumber
            self.cityTextField.text = city
            self.postalnumberTextField.text = postalNumber
            if let email = Auth.auth().currentUser?.email {self.emailLabel.text = "Sähköposti: " + email}
            self.allowShareMyInformationSwitchButton.setOn(allowShareInfo, animated: false)
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        //haetaan kuva palvelimelta
        getPictureFromStorage()
        
    }
    
    func getPictureFromStorage(){
        
        //tulostetaan brand ja model appiin
        let downloadImageRef = self.imageReference.child(filename)
        
        let downloadtask = downloadImageRef.getData(maxSize: 1024 * 1024 * 12) { (data, error) in
            if let data = data {
                let image = UIImage(data: data)
                self.imgProfilePicture.image = image
            }
            print(error ?? "NO ERROR")
        }
        
        downloadtask.observe(.progress) { (snapshot) in
            print(snapshot.progress ?? "NO MORE PROGRESS")
        }
        /*
         downloadtask.resume()
         */
    }
    
    func addPictureToStorage(){
        
        //Lisätään kuva tietokantaan
        guard let image = imgProfilePicture.image else {return}
        guard let imageData = image.jpegData(compressionQuality: 0) else {return}//kuvan laatu 0-1 asteikolla (yksi paras)
        
        let uploadImageRef = imageReference.child(filename)
        
        let uploadTask = uploadImageRef.putData(imageData, metadata: nil) { (metadata,error) in
            print("UPLOAD TASK FINISHED")
            print(metadata ?? "PROFILEPICTURE")
            print ( error ?? "NO ERROR")
            //TODO: tee referenssi
        }
        
        uploadTask.observe(.progress) {(snapshot) in
            print(snapshot.progress ?? "NO MORE PROGRESS")
        }
        
        uploadTask.resume()
        
    }
    

}
