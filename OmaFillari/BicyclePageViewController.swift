//
//  BicyclePageViewController.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 11/01/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class BicyclePageViewController: UIViewController {
    
    
    //@IBOutlet weak var frameNumberLbl: UILabel!
    //@IBOutlet weak var addBicycleButton: UIBarButtonItem!
    //@IBOutlet weak var brandTextField: UILabel!
    //@IBOutlet weak var modelTextField: UILabel!
    //@IBOutlet weak var bicycleImageImageView: UIImageView!
    
    //tietokanta muuttuja
    //var ref: DatabaseReference!
    //storage muuttuja
    //var bicycleFrameNumber:String = ""
    //var imageReference: StorageReference{
    //    return Storage.storage().reference().child("bicycleImages")
    //}
    
    let brandLabel: UILabel = {
        let label = UILabel()
        label.text = "Merkki"
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1) //color literal
        label.font = UIFont.boldSystemFont(ofSize: 27)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let modelLabel: UILabel = {
        let label = UILabel()
        label.text = "Malli"
        label.textAlignment = .center
        label.textColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1) //color literal
        label.font = UIFont.boldSystemFont(ofSize: 27)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //yhdistetään tietokantaan
        //ref = Database.database().reference()
        //updateBicycles()

        // Do any additional setup after loading the view.
        view.addSubview(brandLabel)
        view.addSubview(modelLabel)
        
        setUpLayout()
    }
    
    func setUpLayout(){
        brandLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 10).isActive = true
        brandLabel.centerYAnchor.constraint(equalTo: view.topAnchor).isActive = true
        brandLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        brandLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        modelLabel.topAnchor.constraint(equalTo: brandLabel.bottomAnchor, constant: 20).isActive = true
        modelLabel.centerYAnchor.constraint(equalTo: brandLabel.bottomAnchor).isActive = true
        modelLabel.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        modelLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    /*
    func getOwnBicycleFrameNumber(userid:String){
        
        //haetaan oikeasta polusta kaikki arvot
        ref.child("bicyclesByUsers").child(userid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let value = snapshot.value as? NSDictionary{
                for data in value {
                    self.frameNumberLbl.text = data.value as? String
                    self.bicycleFrameNumber = data.value as! String
                    self.addBicycleButton.isEnabled = false
                }
            }
            //virheen tapahtuessa
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func getBicycleInformationByFrameNumber(){
        //kun pyörän numero on löydetty ja lisätty sivulle, niin tehdään siitä pyörän runkonumero, jolla etsitään loput tiedot pyörästä tietokannasta
        bicycleFrameNumber = "12344321"
        
        //etsitään loput tiedot pyörästä tietokannasta
        ref.child("bicycles").child(bicycleFrameNumber).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            let brand = value?["brand"] as? String ?? "Ei merkkiä"
            let model = value?["model"] as? String ?? "Ei mallia"
            
            //tulostetaan brand ja model appiin
            self.brandTextField.text = brand
            self.modelTextField.text = model
            
            //virheen tapahtuessa
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func updateBicyclePictureByFrameNumber(){
        
        let frameNumber = "12344321"
        ref.child("bicycles").child(frameNumber).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            let bicycleImagePath = value?["bicycleImage"] as? String ?? "Ei merkkiä"
            
            //tulostetaan brand ja model appiin
            let downloadImageRef = self.imageReference.child(bicycleImagePath)
            
            let downloadtask = downloadImageRef.getData(maxSize: 1024 * 1024 * 12) { (data, error) in
                if let data = data {
                    let image = UIImage(data: data)
                    self.bicycleImageImageView.image = image
                }
                print(error ?? "NO ERROR")
            }
            
            downloadtask.observe(.progress) { (snapshot) in
                print(snapshot.progress ?? "NO MORE PROGRESS")
            }
            /*
             downloadtask.resume()
             */
            //virheen tapahtuessa
        }) { (error) in
            print(error.localizedDescription)
        }
        /*
         //TODO: kuvan plendaus
         let maskLayer = CAGradientLayer()
         maskLayer.frame = self.bicycleImageImageView.bounds
         maskLayer.shadowRadius = 10
         maskLayer.shadowPath = CGPath(roundedRect: self.bicycleImageImageView.bounds.insetBy(dx: 5, dy: 5), cornerWidth: 10, cornerHeight: 10, transform: nil)
         maskLayer.shadowOpacity = 1
         maskLayer.shadowOffset = CGSize.zero
         maskLayer.shadowColor = UIColor.white.cgColor
         self.bicycleImageImageView.layer.mask = maskLayer
         */
    }
    
    func updateBicycles() {
        //määritetään userID:ksi käyttäjän iD
        if let userID = Auth.auth().currentUser?.uid{
            getOwnBicycleFrameNumber(userid: userID)
            getBicycleInformationByFrameNumber()
            updateBicyclePictureByFrameNumber()
        }
    }*/

}
