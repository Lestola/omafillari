//
//  pageViewController.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 11/01/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

import Foundation
import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    lazy var viewControllerList:[UIViewController] = {
        let sb = UIStoryboard(name: "Main" , bundle: nil)
        
        let vc1 = sb.instantiateViewController(withIdentifier: "Bike1")
        let vc2 = sb.instantiateViewController(withIdentifier: "Bike2")
        let vc3 = sb.instantiateViewController(withIdentifier: "Bike3")
        let vc4 = sb.instantiateViewController(withIdentifier: "Bike4")
        let vc5 = sb.instantiateViewController(withIdentifier: "Bike5")
        
        return [vc1,vc2,vc3,vc4,vc5]
    }()
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllerList.index(of: viewController) else {return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else {return nil}
        guard viewControllerList.count > previousIndex else {return nil}
        return viewControllerList[previousIndex]
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewControllerList.index(of: viewController) else {return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else {return nil}
        guard viewControllerList.count > nextIndex else {return nil}
        return viewControllerList[nextIndex]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //asetetaan datasource
        self.dataSource = self
        
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    
}
