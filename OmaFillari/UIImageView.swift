//
//  UIImageView.swift
//  OmaFillari
//
//  Created by Marko Lehtola on 29/10/2018.
//  Copyright © 2018 Marko Lehtola. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func roundedImage() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.borderWidth = 4
    }
}
